package com.example.demo;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;
import org.springframework.web.bind.annotation.RestController;

@WebMvcTest(NumberController.class)
public class NumberControllerTest {

    @Autowired
    MockMvc mockMvc;

    @Test
    void computeNumbers_numberArrayAndOperatorArg_returnsResult() throws Exception {
        mockMvc.perform(MockMvcRequestBuilders.get("/number?numbers=1&numbers=2&" +
                        "numbers=3&operator=+"))
                .andExpect(MockMvcResultMatchers.status().isOk())
                .andExpect(MockMvcResultMatchers.content().string("6"));
    }

    @Test
    void computeNumbers_numberArrayAndOperatorArg_returnsResult_Test2() throws Exception {
        mockMvc.perform(MockMvcRequestBuilders.get("/number?numbers=1,2,3,4,5&operator=*"))
                .andExpect(MockMvcResultMatchers.status().isOk())
                .andExpect(MockMvcResultMatchers.content().string("120"));
    }

}
