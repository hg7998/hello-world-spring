package com.example.demo;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class NumberController {

        int result;

        @GetMapping("/number")
        public int computeNumbers(@RequestParam int[] numbers, char operator){
            if (operator == '*') {
                int product = 1;
                for (int i = 0; i < numbers.length; i++) {
                    product *= numbers[i];
                }
                result = product;
            }
            else if (operator == '+') {
                int sum = 0;
                for (int i = 0; i < numbers.length; i++) {
                    sum += numbers[i];
                }
                result = sum;
            }
            else if (operator == '-') {
                int sum = 0;
                for (int i = 0; i < numbers.length; i++) {
                    sum -= numbers[i];
                }
                result = sum;
            }
            else {
                System.out.println("Invalid operator. Please use +, -, or *");
            }
            return result;
        }
    }

