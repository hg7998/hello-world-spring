package com.example.demo;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;
import org.springframework.web.bind.annotation.RestController;

@WebMvcTest(VowelCalculator.class)
public class VowelCalculatorTest {

    @Autowired
    MockMvc mockMvc;

    @Test
    void addVowels_wordArg_returnsResult() throws Exception {
        mockMvc.perform(MockMvcRequestBuilders.get("/vowel?word=elephant"))
                .andExpect(MockMvcResultMatchers.status().isOk())
                .andExpect(MockMvcResultMatchers.content().string("3"));
    }

}
