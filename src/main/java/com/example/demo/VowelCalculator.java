package com.example.demo;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

@RestController
public class VowelCalculator {

    int result;
    List<Character> vowels = Arrays.asList('a', 'e', 'i', 'o', 'u');

    @GetMapping("/vowel")
    public int computeNumbers(@RequestParam String word) {
        for (int i = 0; i < word.length(); i++) {
            if (vowels.contains(word.charAt(i))) {
                result++;
            }
        }
        return result;
    }
}

